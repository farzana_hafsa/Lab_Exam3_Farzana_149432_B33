<?php
$rec_temp = "78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62, 62, 65, 64, 68, 73, 75, 79, 73";
$array_temp = explode(", ",$rec_temp);
$temp_length = count($array_temp);
echo "Average Temperature is : [";
$average = array_sum($array_temp)/ $temp_length;
echo "$average ]<br><br> List of five lowest temperatures : [";
$array_unique = array_unique($array_temp);
sort($array_unique);
for($i=0; $i<=4; $i++){
echo "$array_unique[$i] ";
}
echo "]<br><br>List of five highest temperatures : [";
rsort($array_unique);
for($j=4; $j>=0; $j--){
    echo "$array_unique[$j] ";
}
echo"]";
/* Average Temperature is : [ 70.6 ]

List of five lowest temperatures : [ 60, 62, 63, 63, 64 ]

List of five highest temperatures : [ 76, 78, 79, 81, 85 ]*/
?>